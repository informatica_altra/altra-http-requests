<?php
use Altra\Requests\Dto\CompanyData;
use Altra\Requests\Facades\Company;
use Altra\Requests\Factories\CompanyFactory;
use Altra\Requests\Tests\TestCase;

class CompanyTest extends TestCase
{
  protected $companyFactory;
  public function setUp(): void
  {
    parent::setUp();
    Company::fake();
    $this->companyFactory = new CompanyFactory();
  }

  public function test_create_company()
  {
    $data = $this->companyFactory->definition();
    Company::create(CompanyData::fromArray($data));
    Company::assertCreated($data['fiscal_id']);
  }
}
