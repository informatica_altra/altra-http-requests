<?php

namespace Altra\Requests\Dto;

use Altra\Dto\DataTransfer;
use App\Models\Company;
use Carbon\Carbon;

class CompanyData extends DataTransfer
{
  public function __construct(
    public string | null $altra_company_iso,
    public string | null $codice_fe,
    public string | null $pec,
    public string | null $codice_fiscale,
    public string | null $carrier_service_type_code,
    public int | null $id_a3,
    public string | int | null $taxes,
    public string | null $market,
    public string | null $uuid,
    public string $name,
    public string | null $name_commercial,
    public string | null $fiscal_id,
    public int | null $fiscal_country_id,
    public int | null $origin_crm_id,
    public int | null $crm_id,
    public string | null $main_contact_id,
    public string | null $crm_code,
    public string | null $pay_type,
    public string | null $carrier_incoterms_code,
    public string | null $delivery_cost_code,
    public string | null $state,
    public int $client_profile_id,
    public int | null $is_vat_eu,
    public int | null $discount,
    public string | null $email
  ) {}

  public static function model(): string
  {
    return Company::class;
  }
}
