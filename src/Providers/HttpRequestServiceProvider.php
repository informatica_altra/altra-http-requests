<?php

namespace Altra\Requests\Providers;

use Illuminate\Support\ServiceProvider;

class HttpRequestServiceProvider extends ServiceProvider
{

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {

    $this->publishes([
      __DIR__ . '/../../config/internal_endpoints.php' => config_path('internal_endpoints.php'),
    ]);
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
  }
}
