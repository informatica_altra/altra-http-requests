<?php

namespace Altra\Requests\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
  /**
   * Define the model's default state.
   *
   * @return array<string, mixed>
   */
  public function definition()
  {
    return [
      'altra_company_iso'         => 'ALTRA',
      'codice_fe'                 => null,
      'pec'                       => null,
      'codice_fiscale'            => null,
      'carrier_service_type_code' => null,
      'id_a3'                     => null,
      'taxes'                     => 1.2,
      'market'                    => 'IB',
      'uuid'                      => $this->faker->uuid(),
      'name'                      => $this->faker->firstName,
      'name_commercial'           => $this->faker->firstName,
      'fiscal_id'                 => $this->faker->numerify('123456789'),
      'fiscal_country_id'         => $this->faker->numberBetween(1, 100),
      'origin_crm_id'             => $this->faker->numberBetween(0, 5),
      'crm_id'                    => $this->faker->numberBetween(0, 9999),
      'main_contact_id'           => $this->faker->numberBetween(0, 9999),
      'crm_code'                  => $this->faker->uuid(),
      'pay_type'                  => $this->faker->randomElement([
        'CASH',
        'CREDIT',
      ]),
      'carrier_incoterms_code'    => $this->faker->randomElement([
        'DAP',
        'DDP',
        'EXWORKS',
      ]),
      'delivery_cost_code'        => 'D',
      'state'                     => $this->faker->randomElement([
        'active',
        'pending_activation',
        'disabled',
        'deleted',
      ]),
      'client_profile_id'         => $this->faker->numberBetween(0, 9999),
      'is_vat_eu'                 => 1,
      'discount'                  => 1.5,
      'email'                     => $this->faker->email,
    ];
  }
}
