<?php
namespace Altra\Requests\Services;

use Altra\Requests\Dto\CompanyData;
use Altra\Requests\Traits\HttpRequestTrait;

class CompanyService
{
  use HttpRequestTrait;
  private $url;

  public function __construct()
  {
    $this->url = config('internal_endpoints.mscustomer');
  }

  public function create(CompanyData $data)
  {
    $url = $this->url . '/api/internal/v1/company';
    return $this->makeRequest($url, 'POST', $data->toArray());
  }
}
