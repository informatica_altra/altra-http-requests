<?php
namespace Altra\Requests\Traits;

use Altra\Requests\Exceptions\APIErrorException;
use Illuminate\Support\Facades\Http;

trait HttpRequestTrait
{
  public function makeRequest(string $url, string $method = 'GET', array $data = [])
  {
    $headers = [
      'Accept'       => 'application/json',
      'Content-Type' => 'application/json',
      'apikey'       => env('KONG_API_KEY'),
    ];

    try {
      $response = Http::withHeaders($headers)->$method($url, $data)->json();
      return $this->parseResponse($response);

    } catch (\Throwable$th) {
      throw new APIErrorException($th->getMessage());
    }
  }

  private function parseResponse(mixed $data)
  {
    throw_if(isset($data['result']) && strtolower($data['result']) == 'error', new APIErrorException());
    throw_if(empty($data['body']), new APIErrorException());
    return $data['body'];
  }
}
