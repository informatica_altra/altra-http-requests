<?php

namespace Altra\Requests\Facades;

use Altra\Requests\Fakes\CompanyServiceFake;
use Altra\Requests\Services\CompanyService;
use Illuminate\Support\Facades\Facade;

class Company extends Facade
{

  public static function getFacadeAccessor()
  {
    return CompanyService::class;
  }

  public static function fake()
  {
    static::swap($fake = new CompanyServiceFake);

    return $fake;
  }
}
