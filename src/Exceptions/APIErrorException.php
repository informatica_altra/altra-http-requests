<?php

namespace Altra\Requests\Exceptions;

use Exception;

class APIErrorException extends Exception
{
  public function __construct($message = 'There has been an error in the request to the microservice')
  {
    parent::__construct($message);
  }
}
