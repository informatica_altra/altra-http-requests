<?php

namespace Altra\Requests\Fakes;

use Altra\Requests\Dto\CompanyData;
use Altra\Requests\Services\CompanyService;
use PHPUnit\Framework\Assert as PHPUnit;

class CompanyServiceFake extends CompanyService
{
  protected $createdCompanies = [];
  public function create(CompanyData $data)
  {
    $this->createdCompanies[] = $data;
    return $data;
  }

  public function assertCreated(string $fiscalId)
  {
    $collection = collect($this->createdCompanies);
    $message    = "No se ha creado el company con el fiscal id [{$fiscalId}]";

    PHPUnit::assertTrue($collection->where('fiscal_id', $fiscalId)->isNotEmpty(), $message);
  }
}
